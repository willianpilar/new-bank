package com.brq.newbank.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public abstract class People {
	@Id
	@GeneratedValue ( strategy = GenerationType.SEQUENCE, generator = "people_seq_id" )
	@SequenceGenerator ( sequenceName = "people_seq_id", name = "people_seq_id", allocationSize = 1 )
	private int id;
	
	private String name;
	private int cpf;
	private String gender;
	private int age;
	private String adress;
	private int phone;
	private String email;
}
