package com.brq.newbank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public abstract class Customer extends People {
	@Id
	@GeneratedValue ( strategy = GenerationType.SEQUENCE, generator = "customer_seq_id" )
	@SequenceGenerator ( sequenceName = "customer_seq_id", name = "customer_seq_id", allocationSize = 1 )
	private int id;
	
	private int number_account;
	private int branch;
}
