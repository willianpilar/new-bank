package com.brq.newbank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PersonalAccount extends Customer {
	@Id
	@GeneratedValue ( strategy = GenerationType.SEQUENCE, generator = "personal_account_seq_id" )
	@SequenceGenerator ( sequenceName = "personal_account_seq_id", name = "personal_account_seq_id", allocationSize = 1 )
	private int id;
	
	private String account_type;
	
}
