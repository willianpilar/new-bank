package com.brq.newbank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class BusinessAccount extends Customer {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "business_account_seq_id")
	@SequenceGenerator (sequenceName="business_account_seq_id", name="people_seq_id")
	private int id;
	
	private String company_name;
	private String trading_name;
	private int cnpj;
	private int ie;
}
